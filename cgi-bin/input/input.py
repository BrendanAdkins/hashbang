#!/usr/local/bin/python
print "Content-type: text/html"
print "\n"
print "<pre>"
import os, sys
import cgi
import cgitb
cgitb.enable()
from cgi import escape

print "Your auth type is {0} and you are {1}".format(os.environ["AUTH_TYPE"], os.environ["REMOTE_USER"])

keys = os.environ.keys()
keys.sort()
for k in keys:
    print "%s\t%s" % (escape(k), escape(os.environ[k]))
print "</pre>"