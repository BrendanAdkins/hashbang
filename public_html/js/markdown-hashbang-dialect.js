if (window.markdown) {
	
  var Harshbarn = DialectHelpers.subclassDialect( Gruber ),
      extract_attr = MarkdownHelpers.extract_attr,
      forEach = MarkdownHelpers.forEach;
      
  

  markdown.Harshbarn.prefixed: function prefixed( block, next ) {
    // |    Foo
    // |bar
    // should be a code block followed by a paragraph. Fun
    //
    // There might also be adjacent code block to merge.

    var ret = [],
        re = /^(?: {0,4}\t| {5})(.*)\n?/;

    // 5 spaces + content
    if ( !block.match( re ) )
      return undefined;

    block_search:
    do {
      // Now pull out the rest of the lines
      var b = this.loop_re_over_block(
                re, block.valueOf(), function( m ) { ret.push( m[1] ); } );

      if ( b.length ) {
        // Case alluded to in first comment. push it back on as a new block
        next.unshift( mk_block(b, block.trailing) );
        break block_search;
      }
      else if ( next.length ) {
        // Check the next block - it might be code too
        if ( !next[0].match( re ) )
          break block_search;

        // Pull how how many blanks lines follow - minus two to account for .join
        ret.push ( block.trailing.replace(/[^\n]/g, "").substring(2) );

        block = next.shift();
      }
      else {
        break block_search;
      }
    } while ( true );

    return [ [ "code_block", ret.join("\n") ] ];
  }
  
  
  Markdown.dialects.Harshbarn = Harshbarn;
  Markdown.buildBlockOrder ( Markdown.dialects.Harsharn.block );
  Markdown.buildInlinePatterns( Markdown.dialects.Harsharn.inline );
}