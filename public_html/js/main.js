$(function() {

    var failureString = "Dude I don't *think* that's a real place"

    var pageLoader = {

        "init": function () {
            var username, hashChunks;

            if (window.location.hash.substr(1, 1) === "!") {
                hashChunks = pageLoader.parseHash();
                username = hashChunks.path[0];
            }

            if (username && username !== "") {
                pageLoader.loadUserDefaults(hashChunks);
            } else {
                if (window.location.hash === "") {
                    pageLoader.loadUserDefaults({ path: ['faq'], query: {} });
                } else {
                    pageLoader.display("#mainsection", failureString);
                }
            }
        },

        parseHash: function() {
            var meatAndQuery = window.location.hash.split('?'),
                pairs = {};

            if (meatAndQuery.length > 1) {
                var qstr = meatAndQuery[1].split('&');
                var apair = null;
                $.each(qstr, function(ind, pair) {
                    apair = pair.split('=');
                    pairs[apair[0]] = apair[1];
                });
            }
            var bits = meatAndQuery[0].match(/([a-zA-Z0-9_]*)/g),
                goodbits = [];
            for (var ind in bits) {
                if (bits[ind].length) {
                    goodbits.push(bits[ind]);
                }
            }
            return {path: goodbits, query: pairs};
        },

        loadUserDefaults: function(hashChunks) {
            pageLoader.load(hashChunks.path[0], "#mainsection",
                hashChunks.path[1] ? hashChunks.path[1] : "index",
		failureString
            );
            pageLoader.load(hashChunks.path[0], "#asidesection",
                hashChunks.query.aside || "aside"
            );
            pageLoader.load(hashChunks.path[0], "#pageheader",
                hashChunks.query.header || "header"
            );
            pageLoader.load(hashChunks.path[0], "#pagefooter",
                hashChunks.query.footer || "footer"
            );
            pageLoader.loadStyle(hashChunks.path[0]);
        },

        "load": function(username, sectionid, sourcefile, errormessage) {
            if (errormessage == undefined) {
                errormessage = "";
            }

            $.ajax({
                url: "/~"+username+"/"+sourcefile+".md",
                success: function (data){
                    pageLoader.display(sectionid, data);
                },
                error: function() {
                    $.ajax({
                        url: "/~"+username+"/"+sourcefile+".txt",
                        success: function (data){
                            pageLoader.display(sectionid, data);
                        },
                        error: function() {
                            pageLoader.display(sectionid, errormessage);
                        }
                    });
                }
            });
        },

        "loadStyle": function (username, sourcefile, errmsg) {
            var hashbangCss = $('link[rel~=hashbang]'),
                newUrl = '/~'+username+'/'+
                        (typeof sourcefile === 'undefined' ? 'index' : sourcefile)+
                        '.css';

            if (hashbangCss.length) {
                hashbangCss.attr('href', newUrl);
            } else {
                $('head').append(
                    $('<link rel="stylesheet hashbang" type="text/css" href="'+newUrl+'" />')
                );
            }
        },

        "display": function (sectionid, data) {
            if (window.marked) {
                data = marked(data);
            }

            $(sectionid).html("<p>"+data+"</p>");
        }

    };

    pageLoader.init();

    $(window).on('hashchange',function(){
        pageLoader.init()
    });

});
